package com.hcl.learn.data;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class BookingRestModel {

	private String propertyId;

	private BigDecimal price;
	private Integer quantity;

	private Boolean availability;
}
