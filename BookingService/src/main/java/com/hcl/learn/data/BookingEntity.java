package com.hcl.learn.data;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "bookings")
@Data
public class BookingEntity implements Serializable {

	private static final long serialVersionUID = 5276846679816843860L;

	@Id
	@Column(unique = true)
	private String propertyId;

	private BigDecimal price;
	private Integer quantity;

	private Boolean availability;
}
