package com.hcl.learn;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.learn.data.BookingEntity;
import com.hcl.learn.data.BookingRepository;
import com.hcl.learn.data.BookingRestModel;

@RestController
public class BookingController {

	private final BookingRepository bookingRepository;

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	public BookingController(BookingRepository bookingRepository) {
		this.bookingRepository = bookingRepository;
	}

	@GetMapping("/")
	public String index() {
		return "Welcome to booking service";
	}

	@GetMapping("booking/{propertyId}")
	public BookingRestModel getPricing(@PathVariable("propertyId") String propertyId) {
		BookingEntity bookingEntity = bookingRepository.findById(propertyId)
				.orElseThrow(() -> new IllegalArgumentException("Invalid property Id:" + propertyId));

		BookingRestModel bookingRestModel = modelMapper.map(bookingEntity, BookingRestModel.class);

		return bookingRestModel;
	}
}
